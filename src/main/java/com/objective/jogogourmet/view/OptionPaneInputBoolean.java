package com.objective.jogogourmet.view;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class OptionPaneInputBoolean extends JOptionPane {
	
	private static final long serialVersionUID = -7622601978710833579L;

	public static String showInputDialog(final Object message) {
		
		Object[] options = new String[]{"Sim", "Não"};
        final JOptionPane pane = new JOptionPane(message, QUESTION_MESSAGE,
                                                 OK_CANCEL_OPTION, null,
                                                 options, null);
        final JDialog dialog = pane.createDialog(null, null);
        dialog.setVisible(true);
        final String value = (String) pane.getValue();
        return value;
    }

}
