package com.objective.jogogourmet.view;


import com.objective.jogogourmet.controller.IPerguntaControlle;
import com.objective.jogogourmet.controller.PerguntaFactory;
import com.objective.jogogourmet.model.Prato;
import com.objective.jogogourmet.model.Sessao;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TelaPrincipal {

    private Sessao sessao;
    private IPerguntaControlle perguntaController;
    private Integer contadorCaracteristica = 2;

    public TelaPrincipal(){
        this.sessao = new Sessao();
        this.perguntaController = PerguntaFactory.getFactory();
    }

    public void iniciar(){
        final JFrame frame = new JFrame("Jogo Gourmet");

        frame.setMinimumSize(new Dimension(276, 130));
        frame.getContentPane().setLayout(null);

        JButton btnOk = new JButton("OK");
        btnOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                iniciarPerguntas();
            }
        });
        btnOk.setBounds(85, 45, 89, 23);
        frame.getContentPane().add(btnOk);

        JLabel lblInicial = new JLabel("Pense em um prato que gosta");
        lblInicial.setHorizontalAlignment(SwingConstants.CENTER);
        lblInicial.setBounds(10, 11, 240, 23);
        frame.getContentPane().add(lblInicial);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void iniciarPerguntas(){
        advinhar();
    }

    private void advinhar(){
        String pergunta = perguntaController.perguntarSimOuNaoCaracteristica(sessao, this.contadorCaracteristica);
        String respostaCaracteristica = OptionPaneInputBoolean.showInputDialog(pergunta);
        if(respostaCaracteristica.equals("Sim")){
            tratarRespostaSimCaracteristica();
        }else{
            tratarRespostaNaoCaracteristica();
        }
    }

    private void tratarRespostaSimCaracteristica(){
        String resposta = OptionPaneInputBoolean.showInputDialog(perguntaController.perguntarSimOuNaoPrato(sessao, this.contadorCaracteristica));
        if(resposta.equals("Sim")){
            tratarRespostaSimPrato();
        } else if (resposta.equals("Não")){
            this.contadorCaracteristica += 1;
            tratarRespostaNaoPrato();
        }
    }

    private void tratarRespostaNaoCaracteristica(){
        if(sessao.getNumeroProximaPergunta() == 2 || this.contadorCaracteristica == sessao.getPratos().size()){
            perguntarSeEhBolo();
        }else {
            this.contadorCaracteristica += 1;
            advinhar();
        }
    }

    private void perguntarSeEhBolo(){
        String resposta = OptionPaneInputBoolean.showInputDialog(perguntaController.perguntarSeEhBolo());
        if(resposta.equals("Sim")){
            tratarRespostaSimPrato();
        }else {
            tratarRespostaNaoPrato();
        }
    }

    private void tratarRespostaSimPrato(){
        JOptionPane.showMessageDialog (null, "Acertei de novo!", "Confirm", JOptionPane.INFORMATION_MESSAGE);
    }

    private void tratarRespostaNaoPrato(){
        String respostaPrato = OptionPaneInputText.showInputDialog(perguntaController.perguntarQuePratoPensou());
        String respostaCaracteristica = OptionPaneInputText.showInputDialog(perguntaController.perguntarQualCaracteristica(sessao, respostaPrato));
        Prato prato = new Prato(respostaPrato, respostaCaracteristica);

        this.sessao.setPratoAnterior(respostaPrato);
        this.sessao.setNumeroProximaPergunta(sessao.getNumeroProximaPergunta()+1);
        this.sessao.getPratos().put(sessao.getNumeroProximaPergunta(), prato);
        this.contadorCaracteristica = 2;

    }
}
