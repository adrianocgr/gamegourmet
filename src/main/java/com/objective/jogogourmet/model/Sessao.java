package com.objective.jogogourmet.model;

import java.util.HashMap;
import java.util.Map;

public class Sessao {

    private Map<Integer, Prato> pratos;
    private Integer numeroProximaPergunta;
    private String pratoAnterior;

    public Sessao(){
        this.pratos = new HashMap<Integer, Prato>();
        Prato pratoLasanha = new Prato("Lasanha", "Massa");
        Prato pratoBolo = new Prato("Bolo de Chocolate", "Sobremesa");
        this.pratos.put(1, pratoLasanha);
        this.pratos.put(2, pratoBolo);
        this.numeroProximaPergunta = 2;
        this.pratoAnterior = "Lasanha";
    }

    public Map<Integer, Prato> getPratos() {
        return pratos;
    }

    public void setPratos(Map<Integer, Prato> pratos) {
        this.pratos = pratos;
    }

    public Integer getNumeroProximaPergunta() {
        return numeroProximaPergunta;
    }

    public void setNumeroProximaPergunta(Integer numeroProximaPergunta) {
        this.numeroProximaPergunta = numeroProximaPergunta;
    }

    public String getPratoAnterior() {
        return pratoAnterior;
    }

    public void setPratoAnterior(String pratoAnterior) {
        this.pratoAnterior = pratoAnterior;
    }
}
