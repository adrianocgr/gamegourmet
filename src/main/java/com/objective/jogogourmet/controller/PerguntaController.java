package com.objective.jogogourmet.controller;

import com.objective.jogogourmet.model.Prato;
import com.objective.jogogourmet.model.Sessao;

public class PerguntaController implements IPerguntaControlle {

    public String perguntarSimOuNaoCaracteristica(Sessao sessao, Integer ordem){
        StringBuilder caracteristica = new StringBuilder("O prato que você pensou é ");
        caracteristica.append(getValue(sessao, "C", ordem));
        caracteristica.append("?");
        return caracteristica.toString();
    }

    public String perguntarSimOuNaoPrato(Sessao sessao, Integer ordem){
        StringBuilder prato = new StringBuilder("O prato que você pensou é ");
        prato.append(getValue(sessao, "P", ordem));
        prato.append("?");
        return prato.toString();
    }

    private String getValue(Sessao sessao, String atributo, Integer ordem){
        Integer key = ordem == 2 ? 1 : ordem;

        Prato prato = sessao.getPratos().get(key);
        return "C".equals(atributo) ? prato.getCaracteristica() : prato.getNome();
    }

    public String perguntarSeEhBolo(){
        return "O prato que você pensou é Bolo de Chocolate?";
    }

    public String perguntarQuePratoPensou(){
        return "Qual prato você pensou?";
    }

    public String perguntarQualCaracteristica(Sessao sessao, String novoPrato){
        StringBuilder caracteristica = new StringBuilder(novoPrato);
        caracteristica.append(" é ____________ mas ");
        caracteristica.append(sessao.getPratoAnterior());
        caracteristica.append(" não.");

        return caracteristica.toString();
    }
}
