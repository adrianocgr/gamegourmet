package com.objective.jogogourmet.controller;

import com.objective.jogogourmet.model.Prato;
import com.objective.jogogourmet.model.Sessao;

public interface IPerguntaControlle {
    public String perguntarSimOuNaoCaracteristica(Sessao sessao, Integer ordem);
    public String perguntarSimOuNaoPrato(Sessao sessao, Integer ordem);
    public String perguntarSeEhBolo();
    public String perguntarQuePratoPensou();
    public String perguntarQualCaracteristica(Sessao sessao, String novoPrato);
}
